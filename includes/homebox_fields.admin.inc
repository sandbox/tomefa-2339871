<?php

/**
 * @file
 * Homebox fields admin file.
 */

/**
 * Homebox fields settings form to be add on the homebox settings page.
 */
function homebox_fields_form_homebox_admin_display_form_alter(&$form, &$form_state, $form_id) {

  // Get the arg page pass the homebox_admin_display_form.
  $page = $form_state['build_info']['args'][1];
  // Get the settings for the blocks.
  $settings = (isset($page->settings['blocks']) ? $page->settings['blocks'] : array());

  // Get the homebox category taxonomy.
  $options = array();
  if ($vocabulary = taxonomy_vocabulary_machine_name_load('homebox_category')) {
    if ($terms = taxonomy_get_tree($vocabulary->vid)) {
      foreach ($terms as $term) {
        $options[$term->tid] = str_repeat('-', $term->depth) . $term->name;
      }
    }
  }

  foreach ($terms as $term) {
    $options[$term->tid] = str_repeat('-', $term->depth) . $term->name;
  }

  foreach ($form as &$field) {
    if (isset($field['bid']) && is_array($field)) {
      $block_name = $field['module']['#value'] . '_' . $field['delta']['#value'];
      $field['description'] = array(
        '#type' => 'textarea',
        '#default_value' => (isset($settings[$block_name]['description']) ? $settings[$block_name]['description'] : ''),
      );

      $field['category'] = array(
        '#type' => 'select',
        '#options' => $options,
        '#default_value' => (isset($settings[$block_name]['category']) ? $settings[$block_name]['category'] : ''),
      );

      $field['image'] = array(
        '#type' => 'managed_file',
        '#default_value' => (isset($settings[$block_name]['image']) ? $settings[$block_name]['image'] : ''),
        '#upload_location' => 'public://homebox_images/',
      );
    }
  }

  // Add a custom submit homebox fields admin display form.
  $form['#submit'][] = 'homebox_fields_admin_display_form_submit';

  return $form;
}

/**
 * Homebox fields admin display form submit handler.
 *
 * Add fields variables to the settings of the homebox page.
 */
function homebox_fields_admin_display_form_submit($form, &$form_state) {
  // Fetch page.
  $page = homebox_get_page($form_state['values']['name']);

  foreach ($form_state['values'] as $key => $block) {
    // Check to see if this is a block.
    // We check $block['region'] == 0, we don't want block that are not enabled.
    if (is_array($block) && is_numeric($block['bid']) && $block['bid'] != 0 && is_numeric($block['region']) && (int) $block['region'] > 0) {
      $page->settings['blocks'][$key]['description'] = $block['description'];
      $page->settings['blocks'][$key]['category'] = (int) $block['category'];

      // Change the file to set it to PERMANENT and attached it to the module.
      if (!empty($block['image'])) {
        // Load the file via file.fid.
        $file = file_load($block['image']);
        if ($file->status != FILE_STATUS_PERMANENT) {
          // Change status to permanent.
          $file->status = FILE_STATUS_PERMANENT;
          // Save.
          file_save($file);
          file_usage_add($file, 'homebox_fields', $form_state['values']['name'], $block['bid']);
        }
        $page->settings['blocks'][$key]['image'] = (int) $block['image'];
      }
      else {
        // Get the file usage of this module, type and block.
        $fids = db_select('file_usage')
          ->fields('file_usage', array('fid'))
          ->condition('module', 'homebox_fields')
          ->condition('type', $form_state['values']['name'])
          ->condition('id', $block['bid'])
          ->execute()
          ->fetchAllAssoc('fid');
        foreach ($fids as $fid) {
          // If there is one or more old files, delete them.
          $file = file_load($fid->fid);
          $res = file_delete($file, TRUE);
          if ($res === TRUE) {
            db_delete('file_usage')
              ->condition('fid', $file->fid)
              ->condition('module', 'homebox_fields')
              ->condition('type', $form_state['values']['name'])
              ->condition('id', $block['bid'])
              ->execute();
          }
        }
        $page->settings['blocks'][$key]['image'] = 0;
      }
    }
  }
  // Save settings.
  homebox_save_page($page);
}

/**
 * Process variables for homebox-admin-display.tpl.php.
 */
function template_preprocess_homebox_fields_admin_display_form(&$variables) {
  global $theme_key;
  drupal_add_css(drupal_get_path('module', 'homebox') . '/homebox.css', array(
    'type' => 'module',
    'media' => 'all',
    'preprocess' => TRUE,
  ));

  // Load page.
  if (isset($variables['form']['name']['#value'])) {
    $page = homebox_get_page($variables['form']['name']['#value']);
    $block_regions = homebox_named_columns($page->settings['regions']);
  }
  else {
    $block_regions = homebox_named_columns(0);
  }
  $variables['block_regions'] = $block_regions + array(HOMEBOX_REGION_NONE => t('Disabled'));
  foreach ($block_regions as $key => $value) {
    // Initialize an empty array for the region.
    $variables['block_listing'][$key] = array();
  }

  // Initialize disabled blocks array.
  $variables['block_listing'][BLOCK_REGION_NONE] = array();

  // Set up to track previous region in loop.
  $last_region = '';
  foreach (element_children($variables['form']) as $i) {
    $block = &$variables['form'][$i];
    // Only take form elements that are blocks.
    if (isset($block['info'])) {
      // Fetch region for current block.
      $region = $block['region']['#default_value'];

      // Set special classes needed for table drag and drop.
      $variables['form'][$i]['region']['#attributes']['class'] = array('block-region-select block-region-' . $region);
      $variables['form'][$i]['weight']['#attributes']['class'] = array('block-weight block-weight-' . $region);

      $variables['block_listing'][$region][$i] = new stdClass();
      $variables['block_listing'][$region][$i]->row_class = isset($block['#attributes']['class']) ? $block['#attributes']['class'] : array();
      $variables['block_listing'][$region][$i]->block_modified = isset($block['#attributes']['class']['block-modified']) ? TRUE : FALSE;
      $variables['block_listing'][$region][$i]->block_title = check_plain($block['info']['#value']);
      $variables['block_listing'][$region][$i]->region_select = drupal_render($block['region']) . drupal_render($block['theme']);
      $variables['block_listing'][$region][$i]->title = drupal_render($block['title']);
      $variables['block_listing'][$region][$i]->weight_select = drupal_render($block['weight']);
      $variables['block_listing'][$region][$i]->status = drupal_render($block['status']);
      $variables['block_listing'][$region][$i]->open = drupal_render($block['open']);
      $variables['block_listing'][$region][$i]->movable = drupal_render($block['movable']);
      $variables['block_listing'][$region][$i]->closable = drupal_render($block['closable']);
      $variables['block_listing'][$region][$i]->bid = drupal_render($block['bid']);
      $variables['block_listing'][$region][$i]->printed = FALSE;

      // Custom homebox fields.
      $variables['block_listing'][$region][$i]->category = drupal_render($block['category']);
      $variables['block_listing'][$region][$i]->description = drupal_render($block['description']);
      $variables['block_listing'][$region][$i]->image = drupal_render($block['image']);

      $last_region = $region;
    }
  }

  $variables['form_submit'] = drupal_render_children($variables['form']);
}
