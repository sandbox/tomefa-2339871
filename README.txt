
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Credits


INTRODUCTION
------------

This module is base on the homebox module that allows site administrators
to create dashboards for their users, using blocks as widgets.

But i wanted more custom fields per block to have a better user experience.
With this modules, it added a description field, an image field
and a taxonomy reference field for categorize.


REQUIREMENTS
------------

1. Homebox (http://www.drupal.org/node/380866)


INSTALLATION
------------

1. Install as usual, see http://drupal.org/node/895232 for further information.

2. Go to your Homebox page edit layout page. You will see the new added fields


CREDITS
-------

Current maintainers:
* Tom Blanchard (tomefa - tomefa@gmail.com) - http://www.drupal.org/user/1116410

This project has been sponsored by:
* YOGARIK
  As experts in Open Source web technologies, we provide our expertise
  to customers interested in implementing safe tools tailored
  to assist their company achieve its strategic goals.
  Visit http://www.yogarik.com or http://www.drupal.org/node/1606762
  for more information.
