<?php
/**
 * @file
 * homebox.tpl.php
 * Custom homebox fields layout.
 */
global $user;

$add_links = NULL;
if ($user->uid):
  $add_blocks = array();

  $vocabulary = taxonomy_vocabulary_machine_name_load('homebox_category');
  $terms = taxonomy_get_tree($vocabulary->vid);

  foreach ($page->settings['blocks'] as $delta => $block):
    // If there is no category selected : Set to No category.
    if (!isset($block['category'])) {
      $block['category'] = $terms[0]->tid;
    }
    $add_blocks[$block['category']][] = theme('homebox_fields_block', array(
      'page' => $page,
      'block' => $block,
      'available_blocks' => $available_blocks,
    ));
  endforeach;

  // TODO : check if we have the right translate name.
  /*$terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));*/

  // For all homebox block category, add the list of block.
  foreach ($terms as $term):
    if (isset($add_blocks[$term->tid])):
      $add_links .= theme('homebox_fields_category', array(
        'term' => $term,
        'items' => $add_blocks[$term->tid],
      ));
    endif;
  endforeach;
endif;
?>
<?php global $user; ?>
<div id="homebox" class="<?php print $classes ?> clearfix">
  <?php if ($user->uid): ?>
    <div id="homebox-buttons">
      <?php if (!empty($add_links)): ?>
        <a href="javascript:void(0)" id="homebox-add-link"><?php print t('Add a block') ?></a>
      <?php endif; ?>
      <?php print $save_form; ?>
    </div>
  <?php endif; ?>

  <?php if (!empty($add_links)): ?>
    <div id="homebox-add"><?php print $add_links; ?></div>
  <?php endif; ?>

  <div class="homebox-maximized"></div>
  <?php for ($i = 1; $i <= count($regions); $i++): ?>
    <div class="homebox-column-wrapper homebox-column-wrapper-<?php print $i; ?> homebox-row-<?php print $page->settings['rows'][$i]; ?>"<?php print $page->settings['widths'][$i] ? ' style="width: ' . $page->settings['widths'][$i] . '%;"' : ''; ?>>
      <div class="homebox-column" id="homebox-column-<?php print $i; ?>">
        <?php foreach ($regions[$i] as $key => $weight): ?>
          <?php foreach ($weight as $block): ?>
            <?php if ($block->content): ?>
              <?php print theme('homebox_block', array('block' => $block, 'page' => $page)); ?>
            <?php endif; ?>
          <?php endforeach; ?>
        <?php endforeach; ?>
      </div>
    </div>
  <?php endfor; ?>
</div>
