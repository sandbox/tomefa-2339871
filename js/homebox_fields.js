/**
 * @file
 * Homebox fields javascipt file.
 */

(function($) {

  Drupal.homebox_fields = {
    config: {}
  };

  Drupal.behaviors.homebox_fields = {
    attach: function(context, settings) {
      $('.homebox_category a').click(function () {
        var target = $(this).attr('data-target');
        $('.' + target).toggle();
        return false;
      });
    }
  };

}(jQuery));
